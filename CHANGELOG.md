### Changelog of "*data_project*" Project.

###### 2024-05-02
- Модифицирован pipeline. Логика обобщена при помощи конфигурационных файлов **Hydra**.
- Добавлено логирование параметров моделей и метрик при обучении.

###### 2024-04-29
- В `/models/{data}` загружены модели, обученные при помощи `Snakemake`.

###### 2024-04-29
- Добавлен pipeline предобработки данных и обучения моделей в `/workflow`, реализованный на базе `Snakemake`.
