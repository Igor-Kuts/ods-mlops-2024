FROM python:3.11-slim
# python:3.10-slim

# ARG will be passed to ENV variable via --build-arg parameter
# E.g.: `dockebuild . -t jupyterlab --build-arg with=modelling`
ARG with
ENV with=${with}

# Check if 'with' argument was provided and meets the required condition
RUN if [ -z "$with" ]; then continue; \
    elif [ "$with" != 'modelling' ]; then \
        echo "\n>>> IMAGE BUILD ERROR !!!\n>>> When using [--build-arg], value of 'with' must be 'modelling'"; \
        exit 1; \
    fi

# Set up the working directory
WORKDIR /workplace

# Copy Poetry configuration files
COPY ./pyproject.toml ./poetry.lock /workplace/

# Install system dependencies and set up Poetry
# Добавил установку rsync в контейнер
RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends curl git bash unzip rsync \
    && export POETRY_HOME=/opt/poetry \
    && python -m venv $POETRY_HOME \
    && $POETRY_HOME/bin/pip install poetry==1.8.2 \
    && $POETRY_HOME/bin/poetry --version \
    && echo "alias poetry='/PoetryEnv/bin/poetry'" >> ~/.bashrc \
    && . ~/.bashrc \
    && apt-get clean

# Install project dependencies based on the value of 'with'
RUN export POETRY_HOME=/opt/poetry \
    && python -m venv /.venv \
    && . /.venv/bin/activate \
    && if [ "$with" = 'modelling' ]; then \
        $POETRY_HOME/bin/poetry install --with modelling --no-interaction --no-ansi --sync; \
    else \
        $POETRY_HOME/bin/poetry install --no-interaction --no-ansi --sync; \
    fi

# Set the entrypoint for running JupyterLab
# ENTRYPOINT [ "/PoetryEnv/bin/poetry", "run", "jupyter", "lab", "--ip=0.0.0.0", "--port=8888", "--allow-root" ]
# -- Commented to test Linting in CI
