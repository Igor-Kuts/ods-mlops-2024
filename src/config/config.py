import os
from dataclasses import dataclass, field
from typing import Any

from hydra.core.config_store import ConfigStore
from omegaconf import MISSING

# from catboost import CatBoostClassifier # <--for reference -->
os.environ['CPU_CNT'] = str(os.cpu_count())


# ----------- MODEL ----------- #
@dataclass
class ModelDefault:
    """
    Model's configuration
    - _target_                : used to point Hydra to an instantiation target
    - **model_hyperparameters : hyper-parameters which are to be used in training

    Notes:
    - from hydra.utils import instantiate
    - It is possible to override parameters while instantiating
    -   model = instantiate(cfg.model, iterations=42)
    - Reference: https://hydra.cc/docs/advanced/instantiate_objects/overview/
    """

    _target_: Any = 'catboost.CatBoostClassifier'

    depth: int = 4
    iterations: int = 150
    loss_function: str = 'MultiClass'
    task_type: str = 'CPU'
    devices: str = '0'
    verbose: int = 10
    random_state: int = 42


@dataclass
class ModelAdvanced(ModelDefault):
    depth: int = 8
    iterations: int = 300
    learning_rate: float = 0.05
    loss_function: str = 'MultiClass'
    verbose: int = 25
    task_type: str = 'CPU'
    devices: str = '0'


# ----- DATA PREPROCESSING ----- #
@dataclass
class PreprocessingDefault:
    target: str = 'health'
    to_drop: list = field(
        default_factory=lambda: [
            'x_sp',
            'y_sp',
            'spc_latin',
            'nta',
            'state',
            'address',
            'borocode',
            'boro_ct',
            'census tract',
            'zip_city',
            'council district',
            'created_at',
            'problems',
        ]
    )
    encode_binary: list = field(default_factory=lambda: [])


@dataclass
class PreprocessingAdvanced(PreprocessingDefault):
    # Признаки "Yes" / "No"
    encode_binary: list = field(
        default_factory=lambda: [
            'root_grate',
            'root_stone',
            'root_other',
            'trunk_wire',
            'trnk_light',
            'trnk_other',
            'brch_light',
            'brch_shoe',
            'brch_other',
        ]
    )


# -------------- DATA ------------- #
@dataclass
class Data:
    source_url: str = 'new-york-city/ny-2015-street-tree-census-tree-data'
    data_output_dir: str = 'data'
    model_output_dir: str = 'models'
    # Пути до скриптов предобработки и обучения
    preprocess_path: str = 'workflow/scripts/preprocessing.py'
    training_path: str = 'workflow/scripts/training.py'


# ------------ PRETRAIN ----------- #
@dataclass
class PretrainDefault:
    target: str = '${preprocessing.target}'
    alias: str = 'trees'
    tts_ysize: float = 0.25
    cpu_cnt: int = '${oc.env:CPU_CNT}'
    cpu_job_pct: float = 0.25
    random_state: int = '${model.random_state}'
    # --- TODO --- Добавить RAM так же как и CPU
    # stratify: str = ${preprocessing.target}


# ------------ DEFAULTS ------------#
defaults = [
    {'model': 'default'},  # Forces and asks for user input via MISSING
    {'data': 'data'},
    {'preprocessing': 'default'},
    {'pretrain': 'default'},
]


@dataclass
class Config:
    defaults: list[Any] = field(default_factory=lambda: defaults)
    pretrain: Any = MISSING
    data: Any = MISSING
    preprocessing: Any = MISSING
    model: Any = MISSING


cs = ConfigStore.instance()
cs.store(group='data', name='data', node=Data)
cs.store(group='pretrain', name='default', node=PretrainDefault)
cs.store(group='preprocessing', name='default', node=PreprocessingDefault)
cs.store(group='preprocessing', name='advanced', node=PreprocessingAdvanced)
cs.store(group='model', name='default', node=ModelDefault)
cs.store(group='model', name='advanced', node=ModelAdvanced)
cs.store(name='config', node=Config)
# '${preprocessing.target}' : Variable Interpolation
