import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def corr_heatmap(
    data: pd.DataFrame,
    target: str = None,
    absolute: bool = False,
    figsize: tuple[int, int] = (20, 15),
    fontsize: int = 8,
    cmap: str = 'vlag',
) -> pd.DataFrame:
    """
    Вспомогательная функция для построения тепловых карт попарных корреляций.

    Args:
        data (pd.DataFrame): Данные
        target (str, optional): Целевая переменная.
        absolute (bool, optional): Флаг для отображения абсолютных значений.
        figsize (tuple[int], optional): Размеры изображения. По умолчанию (20, 15).
        fontsize (int, optional): Размер шрифта в ячейках карты. По умолчанию 8.
        cmap (str, optional): Название используемой тепловой карты
            matplotlib или seaborn. По умолчанию 'vlag'.

    Returns:
        pd.DataFrame: Датафрейм попарных корреляций.
    """
    from matplotlib.colors import TwoSlopeNorm

    corr_matrix = data.corr().abs() if absolute else data.corr()
    corr_matrix = round(corr_matrix, 1).replace(-0.0, 0)

    fig, ax = plt.subplots(figsize=figsize)
    norm = TwoSlopeNorm(vmin=-1.2, vcenter=0, vmax=1.2)
    ax = sns.heatmap(
        corr_matrix,
        annot=True,
        annot_kws={'fontsize': fontsize},
        fmt='0.1f',
        cmap=cmap,
        ax=ax,
        lw=0.5,
        linecolor='black',
        norm=norm,
        square=True,
    )
    if target:
        target_rows = corr_matrix.index[corr_matrix.index.str.contains(target)]
        target_columns = corr_matrix.columns[corr_matrix.columns.str.contains(target)]

        for i, row_name in enumerate(corr_matrix.index):
            if row_name in target_rows:
                ax.add_patch(
                    plt.Rectangle(
                        (i, 0), 1, data.shape[1], fill=False, edgecolor='black', lw=4
                    )
                )
        for j, col_name in enumerate(corr_matrix.columns):
            if col_name in target_columns:
                ax.add_patch(
                    plt.Rectangle(
                        (0, j), data.shape[0], 1, fill=False, edgecolor='black', lw=4
                    )
                )

    ax.add_patch(
        plt.Rectangle(
            (corr_matrix.shape[1], 0),
            1,
            corr_matrix.shape[0],
            fill=False,
            edgecolor='black',
            lw=2,
        )
    )
    ax.add_patch(
        plt.Rectangle(
            (0, corr_matrix.shape[0]),
            corr_matrix.shape[1],
            1,
            fill=False,
            edgecolor='black',
            lw=2,
        )
    )
    plt.show()
    return corr_matrix


def map_binary(
    data: pd.DataFrame, column: str, cat_names: list[str], to_map: tuple = (1, 0)
) -> pd.Series:
    """Функция для маппинга категориальных бинарных признаков.

    Args:
        data (pd.DataFrame): Данные.
        column (str): Название столбца, содержащего бинарный признак.
        cat_names (list[str]): Список с названиями категорий.
        to_map (tuple, optional): Порядок маппинга. По умолчанию (1, 0).

    Returns:
        pd.Series: Столбец Pandas с преобразованными значениями.
    """
    # Mapping procedure
    feature = data[column].copy()
    feature.loc[feature == cat_names[0]] = to_map[0]
    feature.loc[feature == cat_names[1]] = to_map[1]

    # Cast from an "object" to int8 or float16, if NaN present
    feature = (
        feature.astype('float16')
        if feature.isna().sum() > 0
        else feature.astype('int8')
    )
    return feature


def map_tertiary(
    data: pd.DataFrame, column: str, cat_names: list[str], to_map: tuple = (0, 1, 2)
) -> pd.Series:
    """Функция для маппинга категориальных третичных признаков.

    Args:
        data (pd.DataFrame): Данные.
        column (str): Название столбца, содержащего третичный признак.
        cat_names (list[str]): Список с названиями категорий.
        to_map (tuple, optional): Порядок маппинга. По умолчанию (0, 1, 2).

    Returns:
        pd.Series: Столбец Pandas с преобразованными значениями.
    """
    # Mapping procedure
    feature = data[column].copy()
    feature.loc[feature == cat_names[0]] = to_map[0]
    feature.loc[feature == cat_names[1]] = to_map[1]
    feature.loc[feature == cat_names[2]] = to_map[2]

    # Cast from an "object" to int8 or float16, if NaN present
    feature = (
        feature.astype('float16')
        if feature.isna().sum() > 0
        else feature.astype('int8')
    )
    return feature
