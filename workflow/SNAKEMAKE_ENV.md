По какой-то неизвестной причине - не удаётся корректно установить зависимости в окружение с помощью `.yml` файла:
```
name: snakemake-env
channels:
  - bioconda
  - conda-forge
dependencies:
  - snakemake
  - catboost
  - joblib
  - kaggle
  - pandas
  - scikit-learn
```
Poetry не может установить Snakemake из-за проблем с пакетом `datri`.

По-этому, будем устанавливать окружение для Snakemake через CLI.
Из корня проекта выполните:
- `micromamba create -c conda-forge -c bioconda -n hydrasnakemake-env bioconda::snakemake conda-forge::hydra-core catboost joblib kaggle pandas scikit-learn matplotlib seaborn tabulate -y`

Запустите `snakemake` командой:
- `micromamba run -n hydrasnakemake-env snakemake --cores all`
