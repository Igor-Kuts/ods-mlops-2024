#! /bin/bash

micromamba run kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data
unzip ./ny-2015-street-tree-census-tree-data.zip 2015-street-tree-census-tree-data.csv -d ./data -y
rm ./ny-2015-street-tree-census-tree-data.zip
mv  ./data/2015-street-tree-census-tree-data.csv ./data/2015-street-tree-census-tree-data.raw.csv
