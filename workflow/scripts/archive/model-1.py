import sys

import joblib
import pandas as pd
from catboost import CatBoostClassifier
from sklearn.model_selection import train_test_split

# Add the parent and current directory to the Python path
sys.path.append('../')
sys.path.append('../../')
print('>>> Warming up with Model-1')

data = pd.read_csv(snakemake.input[0])

data = data.set_index('tree_id')

# Обработаем столбец с целевой переменной (Здоровье дерева)
data.health = data.health.astype('object')

X = data.drop('health', axis=1)
y = data.health

cat_features = X.select_dtypes('object').columns.to_list()
X[cat_features] = X[cat_features].fillna('NONE')
X[cat_features] = X[cat_features].astype('category')

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, stratify=y, random_state=42
)

params = {
    'depth': 8,
    'iterations': 300,
    'loss_function': 'MultiClass',
    'verbose': 25,
    'task_type': 'CPU',
    'devices': '0',
}

cb = CatBoostClassifier(**params)
print(f">>> Training {cb.__dict__['_object']}, on {snakemake.input[0]}")
print(f"--- with params: {cb.__dict__['_init_params']}")
cb.fit(
    X_train,
    y_train,
    cat_features=cat_features,
)

print(f'>>> Dumping trained Model-1 as {snakemake.output[0]}')
joblib.dump(cb, snakemake.output[0])
