import os
import sys

import pandas as pd

# Add the parent and current directory to the Python path
sys.path.append(os.path.dirname('preprocessing-1.py'))
sys.path.append('../')
sys.path.append('../../')

from src.utils.data_wrangling import map_binary, map_tertiary

data = pd.read_csv(snakemake.input[0])  # snakemake.input[0]

# Оставим только те наблюдения, у которых есть отметка о Health
data = data[~data.health.isna()]

# -------- Вручную закодируем некоторые категориальные признаки -------- #
binary_features = [
    'root_grate',
    'root_stone',
    'root_other',
    'trunk_wire',
    'trnk_light',
    'trnk_other',
    'brch_light',
    'brch_shoe',
    'brch_other',
]
for feature in binary_features:
    data[feature] = map_binary(data=data, column=feature, cat_names=['Yes', 'No'])
data['sidewalk'] = map_binary(
    data=data, column='sidewalk', cat_names=['Damage', 'NoDamage']
)
data['curb_loc'] = map_binary(
    data=data, column='curb_loc', cat_names=['OnCurb', 'OffsetFromCurb']
)

data['status'] = map_tertiary(
    data=data, column='status', cat_names=['Dead', 'Stump', 'Alive']
)
data['health'] = map_tertiary(
    data=data, column='health', cat_names=['Poor', 'Fair', 'Good']
)
# Nan входит в список допустимых значений: отсутствие признаков ухода за деревом = 0
data['steward'] = map_tertiary(
    data=data, column='steward', cat_names=['1or2', '3or4', '4orMore'], to_map=(1, 2, 3)
)
data['steward'] = data['steward'].fillna(0).astype('int8')

# -------- Удалим не нужные признаки -------- #

data = data.drop(
    [
        'x_sp',
        'y_sp',
        'spc_latin',
        'nta',
        'state',
        'address',
        'borocode',
        'boro_ct',
        'census tract',
        'zip_city',
        'council district',
        'created_at',
        'problems',
    ],
    axis=1,
)

# -------- Сохраним обработанные данные -------- #
data.to_csv(snakemake.output[0], index=False)
