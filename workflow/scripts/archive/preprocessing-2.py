import os
import sys

import pandas as pd

# Add the parent and current directory to the Python path
sys.path.append(os.path.dirname(os.path.abspath('preprocessing-2.py')))
sys.path.append('../')
sys.path.append('../../')

data = pd.read_csv(snakemake.input[0])

# Оставим только те наблюдения, у которых есть отметка о Health
data = data[~data.health.isna()]

# -------- Не будем кодировать категориальные признаки -------- #

# -------- Удалим не нужные признаки -------- #

data = data.drop(
    [
        'x_sp',
        'y_sp',
        'spc_latin',
        'nta',
        'state',
        'address',
        'borocode',
        'boro_ct',
        'census tract',
        'zip_city',
        'council district',
        'created_at',
        'problems',
    ],
    axis=1,
)

# -------- Сохраним обработанные данные -------- #
data.to_csv(snakemake.output[0], index=False)
