import os
import sys

import hydra
import pandas as pd
from hydra.core.config_store import ConfigStore

# Add the parent and current directory to the Python path
sys.path.append(os.path.dirname(os.path.abspath('preprocessing.py')))
sys.path.append('../')
sys.path.append('../../')

from src.utils.data_wrangling import map_binary, map_tertiary
from src.config.config import (
    Config,
    Data,
    PreprocessingDefault, PreprocessingAdvanced
)


# Инициализируем конфигурацию
cs = ConfigStore.instance()

cs.store(group='data', name='data', node=Data)
cs.store(group='preprocessing', name='default', node=PreprocessingDefault)
cs.store(group='preprocessing', name='advanced', node=PreprocessingAdvanced)

cs.store(name='config', node=Config)

# Пробросим через CLI информацию из Rule
snakemake_input, snakemake_output = os.getenv('INPUT'), os.getenv('OUTPUT')

@hydra.main(version_base=None, config_name='config')
def main(cfg: Config) -> pd.DataFrame:

    data = pd.read_csv(snakemake_input)  # snakemake.input[0]

    # Оставим только те наблюдения, у которых есть отметка о Health
    data = data[~data[cfg.preprocessing.target].isna()]

    # -------- Закодируем некоторые категориальные признаки -------- #
    binary_features = cfg.preprocessing.encode_binary

    for feature in binary_features:
        data[feature] = map_binary(data=data, column=feature, cat_names=['Yes', 'No'])
    data['sidewalk'] = map_binary(
        data=data, column='sidewalk', cat_names=['Damage', 'NoDamage']
    )
    data['curb_loc'] = map_binary(
        data=data, column='curb_loc', cat_names=['OnCurb', 'OffsetFromCurb']
    )

    data['status'] = map_tertiary(
        data=data, column='status', cat_names=['Dead', 'Stump', 'Alive']
    )
    data['health'] = map_tertiary(
        data=data, column='health', cat_names=['Poor', 'Fair', 'Good']
    )
    # Nan входит в список допустимых значений: отсутствие признаков ухода за деревом = 0
    data['steward'] = map_tertiary(
        data=data, column='steward', cat_names=['1or2', '3or4', '4orMore'], to_map=(1, 2, 3)
    )
    data['steward'] = data['steward'].fillna(0).astype('int8')

    # -------- Удалим не нужные признаки -------- #
    # snakemake.output[0] - in case of using "script" in rule
    data = data.drop(cfg.preprocessing.to_drop, axis=1)

    # -------- Сохраним обработанные данные -------- #
    data.to_csv(snakemake_output, index=False)


if __name__ == '__main__':
    main()
