import os
import sys

import joblib
import logging
import pandas as pd
from tabulate import tabulate

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score

import hydra
from hydra.core.config_store import ConfigStore
from hydra.utils import instantiate

# Add the parent and current directory to the Python path
sys.path.append(os.path.dirname(os.path.abspath('training.py')))
sys.path.append('../')
sys.path.append('../../')

from src.config.config import (
    Config,
    Data,
    PretrainDefault,
    ModelDefault, ModelAdvanced,
    PreprocessingDefault, PreprocessingAdvanced,
)


# Инициализируем логгер
log = logging.getLogger(__name__)

# Инициализируем конфигурацию
cs = ConfigStore.instance()
cs.store(group='data', name='data', node=Data)
cs.store(group='pretrain', name='default', node=PretrainDefault)
cs.store(group='preprocessing', name='default', node=PreprocessingDefault)
cs.store(group='preprocessing', name='advanced', node=PreprocessingAdvanced)
cs.store(group='model', name='default', node=ModelDefault)
cs.store(group='model', name='advanced', node=ModelAdvanced)

# Пробросим через CLI информацию из Rule
snakemake_input, snakemake_output = os.getenv('INPUT'), os.getenv('OUTPUT')
model_type = os.getenv('MODEL')

@hydra.main(version_base=None, config_name='config')
def train(cfg: Config) -> pd.DataFrame:

    print(f">>> Warming up with Model-{model_type}")
    # print('PASSED PARAM:', snakemake.params.param, snakemake.params.pampam)
    data = pd.read_csv(snakemake_input)

    data = data.set_index('tree_id')

    # Обработаем столбец с целевой переменной (Здоровье дерева)
    data[cfg.pretrain.target] = data[cfg.pretrain.target].astype('object')

    X = data.drop([cfg.pretrain.target], axis=1)
    y = data[cfg.pretrain.target]
    y = y.astype('int32')
    cat_features = X.select_dtypes('object').columns.to_list()
    X[cat_features] = X[cat_features].fillna('NONE')
    X[cat_features] = X[cat_features].astype('category')

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=cfg.pretrain.tts_ysize, stratify=y,
        random_state=cfg.pretrain.random_state,
    )

    model = instantiate(cfg.model)

    print(f">>> Training {model.__dict__['_object']}, on {snakemake_input}")
    print(f"--- with params: {model.__dict__['_init_params']}")
    model.fit(
        X_train,
        y_train,
        cat_features=cat_features,
    )

    # Получим предсказания модели, посчитаем ROC-AUC и логируем результат
    y_prob = model.predict_proba(X_test)
    auroc_ovr = roc_auc_score(y_test, y_prob, multi_class='ovr').round(3)
    auroc_ovo = roc_auc_score(y_test, y_prob, multi_class='ovo').round(3)
    
    log_message = '\n' \
        f"--- MODEL: {snakemake_output}\n" \
        f"--- ROC-AUC [OvR] | [OvO] : {auroc_ovr} | {auroc_ovo}\n" \
        f"{tabulate({key: [val] for key, val in cfg.model.items()}, headers='keys', tablefmt='psql')}"
    log.info(log_message)

    joblib.dump(model, snakemake_output)


if __name__ == '__main__':
    train()
